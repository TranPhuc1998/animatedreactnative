import {FontAwesome, Ionicons} from '../../../Assets/VectorIcons/Icons';
const colorRenderTabs = ['red', 'violet', 'green', 'skyblue'];
const dataTab = [
  {id: 1, title: 'Trang chủ', TypeIcon: FontAwesome, icon: 'home'},
  {id: 2, title: 'Tìm kiếm', TypeIcon: FontAwesome, icon: 'search'},
  {
    id: 3,
    title: 'Thông báo',
    TypeIcon: Ionicons,
    icon: 'md-notifications-sharp',
  },
  {id: 4, title: 'Cá nhân', TypeIcon: FontAwesome, icon: 'user'},
];
export {colorRenderTabs, dataTab};
